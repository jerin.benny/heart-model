#include<cmath>
#include<vector>
#include<iostream>
#include<fstream>
#include<string>
#include<stdlib.h>
using namespace std;
const double pi = 3.14159265358979323846;
double abstol = 0.0000001;
double reltol=0.0000000001;
double yo_iter[22],temp_1[22];
double* y;
double* temp;
double* F_iter;
double Parameters[61];
double ea2 (double t, double RR, double Tac){
	double N=floor(t/RR);
	double T=t-N*RR;
	double EA;
	if (0 <= T && T <= Tac){
		EA=0;
	}
	else if (Tac < T && T < RR){
		EA = 1 - cos((T - Tac)/(RR - Tac)*2*pi);
	}
	else{
		EA=0;
	}
	return EA;
}
double ev2 (double t, double RR, double Tce, double Tme){
	double N=floor(t/RR);
	double T=t-N*RR;
	double EV;
	if (0 <= T && T < Tme){
		EV = 1 - cos(T/Tme*pi);
	}
	else if (T >= Tme && T < Tce){
		EV = 1 + cos((T - Tme)/(Tce - Tme)*pi);
	}
	else if (T >= Tce && T < RR){
		EV=0;
	}
	else{
		EV=0;
	}
	return EV;
}
double* ft(double t, double *y){
	double Vla = y[0]; 
	double Vlv = y[1];
	double Vra = y[2];
	double Vrv = y[3];
	double Psas = y[4];
	double Qsas = y[5];
	double Psat = y[6];
	double Qsat = y[7];
	double Psvn = y[8];
	double Ppas = y[9];
	double Qpas = y[10];
	double Ppat = y[11];
	double Qpat = y[12];
	double Ppvn = y[13];
	double Omi = y[14];
	double Wmi = y[15]; 
	double Oao = y[16];
	double Wao = y[17]; 
	double Oti = y[18];
	double Wti = y[19];
	double Opo = y[20];
	double Wpo = y[21];

	double RR = 0.8;

	double  CQao = Parameters[0];
	double  CQmi = Parameters[1];
	double  Elvmax = Parameters[2];
	double  Elvmin = Parameters[3];
	double  Plvun = Parameters[4];
	double  Vlvun = Parameters[5];
	double  Elamax = Parameters[6];
	double  Elamin = Parameters[7];
	double  Plaun = Parameters[8];
	double  Vlaun = Parameters[9];
	double  CQpo = Parameters[10];
	double  CQti = Parameters[11];
	double  Ervmax = Parameters[12];
	double  Ervmin = Parameters[13];
	double  Prvun = Parameters[14];
	double  Vrvun = Parameters[15];
	double  Eramax = Parameters[16];
	double  Eramin = Parameters[17];
	double  Praun = Parameters[18];
	double  Vraun = Parameters[19];

	double  Csas = Parameters[20];
	double  Rsas = Parameters[21];
	double  Lsas = Parameters[22];
	double  Psasun = Parameters[23];
	double  Vsasun = Parameters[24];
	double  Csat = Parameters[25];
	double  Rsat = Parameters[26];
	double  Lsat = Parameters[27];
	double  Psatun = Parameters[28];
	double  Vsatun = Parameters[29];
	double  Rsar = Parameters[30];
	double  Rscp = Parameters[31];
	double  Rsvn = Parameters[32];
	double  Csvn = Parameters[33];
	double  Psvnun = Parameters[34];
	double  Vsvnun = Parameters[35];

	double  Cpas = Parameters[36];
	double  Rpas = Parameters[37];
	double  Lpas = Parameters[38];
	double  Ppasun = Parameters[39];
	double  Vpasun = Parameters[40];
	double  Cpat = Parameters[41];
	double  Rpat = Parameters[42];
	double  Lpat = Parameters[43];
	double  Ppatun = Parameters[44];
	double  Vpatun = Parameters[45];
	double  Rpar = Parameters[46];
	double  Rpcp = Parameters[47];
	double  Rpvn = Parameters[48];
	double  Cpvn = Parameters[49];
	double  Ppvnun = Parameters[50];
	double  Vpvnun = Parameters[51];

	double  Kpmi = Parameters[52];
	double  Kpao = Parameters[52];
	double  Kpti= Parameters[52];
	double  Kppo = Parameters[52];
	double  Kfmi = Parameters[53];
	double  Kfao = Parameters[53];
	double  Kfti = Parameters[53];
	double  Kfpo = Parameters[53];
	double  Kbmi = Parameters[54];
	double  Kbao = Parameters[54];
	double  Kbti = Parameters[54];
	double  Kbpo = Parameters[54];
	double  Kvmi = Parameters[55];
	double  Kvti = Parameters[55];
	double  Kvao = Parameters[56];
	double  Kvpo = Parameters[56];
	double  Omax = Parameters[57];

	double  Tac = Parameters[58];
	double  Tme = Parameters[59];
	double  Tce = Parameters[60];


	double Ela = Elamin + (Elamax - Elamin)/2 * ea2(t , RR, Tac);    
	double  Pla = Plaun + Ela*(Vla - Vlaun);

	double  Elv = Elvmin + (Elvmax - Elvmin)/2 * ev2(t, RR, Tce, Tme);
	double  Plv = Plvun + Elv*(Vlv - Vlvun);

	double  Era = Eramin + (Eramax - Eramin)/2 * ea2(t, RR, Tac);
	double  Pra = Praun + Era*(Vra - Vraun);

	double  Erv = Ervmin + (Ervmax - Ervmin)/2 * ev2(t, RR, Tce, Tme);
	double  Prv = Prvun + Erv*(Vrv - Vrvun);

	double  Vsas = Vsasun + (Psas - Psasun)*Csas;
	double  Vsat = Vsatun + (Psat - Psatun)*Csat;
	double  Vsvn = Vsvnun + (Psvn - Psvnun)*Csvn;

	double  Vpas = Vpasun + (Ppas - Ppasun)*Cpas;
	double  Vpat = Vpatun + (Ppat - Ppatun)*Cpat;
	double  Vpvn = Vpvnun + (Ppvn - Ppvnun)*Cpvn;
	double ARmi;
	double Qmi;
	double ARao;
	double Qao;
	double ARti;
	double Qti;
	double ARpo;
	double Qpo;
	if (Pla >= Plv){
	ARmi = pow((1 - cos(Omi)),2)/pow((1 - cos(Omax)),2);
	Qmi = CQmi*ARmi*pow((Pla - Plv),0.5);
	}
	else if (Pla < Plv){
	ARmi = 0;
	Qmi = 0;
	}

	if (Plv >= Psas){
	ARao = pow((1 - cos(Oao)),2)/pow((1 - cos(Omax)),2);
	Qao = CQao*ARao*pow((Plv - Psas),0.5);
	}
	else if (Psas > Plv){
	ARao = 0;
	Qao = 0;
	}

	if (Pra >= Prv){
	ARti = pow((1 - cos(Oti)),2)/pow((1 - cos(Omax)),2);
	Qti = CQti*ARti*pow((Pra - Prv),0.5);
	}
	else if (Prv > Pra){
	ARti = 0;
	Qti = 0;
	}

	if (Prv >= Ppas){
	ARpo = pow((1 - cos(Opo)),2)/pow((1 - cos(Omax)),2);
	Qpo = CQpo*ARpo*pow((Prv - Ppas),0.5);
	}
	else if (Ppas > Prv){
	ARpo = 0;
	Qpo = 0;
	}

	double Qsvn = (Psvn - Pra)/Rsvn;
	double Qpvn = (Ppvn - Pla)/Rpvn;
	double F[22];
	F[0] = Qpvn - Qmi;
	F[1] = Qmi - Qao; 
	F[2] = Qsvn - Qti;
	F[3] = Qti - Qpo; 
	F[4] = (Qao - Qsas)/Csas; 
	F[5] = (Psas - Psat - Rsas*Qsas)/Lsas; 
	F[6] = (Qsas - Qsat)/Csat; 
	F[7] = (Psat - Psvn - (Rsat + Rsar + Rscp)*Qsat)/Lsat;
	F[8] = (Qsat - Qsvn)/Csvn;
	F[9] = (Qpo - Qpas)/Cpas;
	F[10] = (Ppas - Ppat - Rpas*Qpas)/Lpas;
	F[11] = (Qpas - Qpat)/Cpat;
	F[12] = (Ppat - Ppvn - (Rpat + Rpar + Rpcp)*Qpat)/Lpat;
	F[13] = (Qpat - Qpvn)/Cpvn;    
	F[14] = Wmi; 
	F[15] = (Pla - Plv)*Kpmi*cos(Omi) - Kfmi*Wmi  + Kbmi*Qmi*cos(Omi) - Kvmi*Qmi*sin(2*Omi);

	F[16] = Wao;
	F[17] = (Plv - Psas)*Kpao*cos(Oao) - Kfao*Wao  + Kbao*Qao*cos(Oao) - Kvao*Qao*sin(2*Oao);

	F[18] = Wti;
	F[19] = (Pra - Prv)*Kpti*cos(Oti) - Kfti*Wti  + Kbti*Qti*cos(Oti) - Kvti*Qti*sin(2*Oti);

	F[20] = Wpo;
	F[21] = (Prv - Ppas)*Kppo*cos(Opo) - Kfpo*Wpo  + Kbpo*Qpo*cos(Opo) - Kvpo*Qpo*sin(2*Opo);
	F_iter=F;
	return F_iter;
}
struct respr{
	vector<double>tme;
	vector<double> volv;
	vector<double> prlv;
}integ;
void resapp(double *y0, double t){
	double  Vlv = y0[1];
	double  Elvmax = Parameters[2];
	double  Elvmin = Parameters[3];
	double  Tme = Parameters[59];
	double  Tce = Parameters[60];
	double  Plvun = Parameters[4];
	double  Vlvun = Parameters[5];
	double  RR = 0.8;
	double  Elv = Elvmin + (Elvmax - Elvmin)/2 * ev2(t, RR, Tce, Tme);
	double  Plv = Plvun + Elv*(Vlv - Vlvun);
	integ.volv.push_back(Vlv);
	integ.prlv.push_back(Plv);
	integ.tme.push_back(t);
}
double integrate(double t,double *y,double h){
	double k1[22],k2[22],k3[22],k4[22],k5[22],k6[22],k7[22],temp_2[22],scale[22],delta[22],temp[22];
	double* A;
	int i;
	for (i=0;i<22;i++){
	A=ft(t,y);
	k1[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+k1[i]/5;
	}
	for (i=0;i<22;i++){
	A=ft(t+h/5,temp);
	k2[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+3*k1[i]/40+9*k2[i]/40;
	}
	for (i=0;i<22;i++){
	A=ft(t+3*h/10,temp);
	k3[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+44*k1[i]/45-56*k2[i]/15+32*k3[i]/9;
	}
	for (i=0;i<22;i++){
	A=ft(t+4*h/5,temp);
	k4[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+19372*k1[i]/6561-25630*k2[i]/2187+64448*k3[i]/6561-212*k4[i]/729;
	}
	for (i=0;i<22;i++){
	A=ft(t+8*h/9,temp);
	k5[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+9017*k1[i]/3168-355*k2[i]/33+46732*k3[i]/5247+49*k4[i]/176-5103*k5[i]/18656;
	}
	for (i=0;i<22;i++){
	A=ft(t+h,temp);
	k6[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp[i]=y[i]+35*k1[i]/384+500*k3[i]/1113+125*k4[i]/192-2187*k5[i]/6784+11*k6[i]/84;
	}
	for (i=0;i<22;i++){
	A=ft(t+h,temp);
	k7[i]=h*A[i];
	}
	for (i=0;i<22;i++){
	temp_1[i]=35*k1[i]/384+500*k3[i]/1113+125*k4[i]/192-2187*k5[i]/6784+11*k6[i]/84;
	temp_2[i]=5179*k1[i]/57600+7571*k3[i]/16695+393*k4[i]/640-92097*k5[i]/339200+187*k6[i]/2100+k7[i]/40;
	}
	double errsqr=0;
	for(i=0;i<22;i++){
		scale[i]=abstol+max(abs(temp_2[i]),abs(temp_1[i]))*reltol;
		delta[i]=abs(temp_1[i]-temp_2[i]);
		//cout<<temp[i]<<"\t"<<temp_1[i]<<"\n";
		errsqr+=(delta[i]*delta[i]/scale[i]/scale[i]);
	}
	//cout<<"\n\n\n\n";
	double err=pow(errsqr,0.5);
	return err;
}
int main(){
	double t=0;
	double h=0.01;
	string a;
	int j=0;
	ifstream qfile;
	qfile.open("param.csv");
	while(qfile.good()&&j<61){
		getline(qfile,a);
		Parameters[j]=atof(a.c_str());
		//cout<<Parameters[j];
		j++;
	}
	qfile.close();
	ifstream pfile;
	pfile.open("y0.csv");
	j=0;
	string g;
	while(pfile.good()&&j<22){
		getline(pfile,g);
		yo_iter[j]=atof(g.c_str());
		y=yo_iter;
		j++;
	}

	double err=integrate(t,y,h);
	//cout<<err<<"\n";
	while (t<=8){
		if (err>1){
			h=0.95*h/(pow(err,0.2));
			//if (h>0.1){
			//	h=0.1;
			//}
			err=integrate(t,y,h);
			//cout<<err<"\n";
		}
		else{
			for (int i=0;i<22;i++){
				y[i]=y[i]+temp_1[i];
			}
			resapp(y,t);
			t=t+h;
			h=0.95*h/(pow(err,0.2));
			//if (h>0.1){
			//	h=0.1;
			//}
			//cout<<t<<"\n";			
			err=integrate(t,y,h);
			//cout<<err<<"\n";
		}
	}
	ofstream rfile;
	rfile.open("test.csv");
	for(std::size_t i=0; i<integ.tme.size(); ++i){
		rfile<<integ.tme[i]<<","<<integ.volv[i]<<","<<integ.prlv[i]<<"\n";
	}	
        rfile.close();
	return 0;
}
